/**
 * Bài 1
 * Đầu vào: lương nhân viên/ 1 ngày và số ngày làm.
 * Các bước xử lý: 
 * + Bước 1 : Tạo biến luongCoBan gán giá trị là 100.000
 * + Bước 2 : Tạo biến soNgayLam cho số ngày làm việc
 * + Bước 3 : Tạo biến tongLuong nhận giá trị là tongLuong = luongCoBan * soNgayLam.
 * + Bước 4 : In kết quả tongLuong theo biểu mẩu ra console.
 * Đầu ra: Tổng lương nhân viên nhận được.
 */
var luongCoBan = 100000.00;
var soNgayLam = 50;
var tongLuong = luongCoBan * soNgayLam;
console.log("Tổng lương:",tongLuong);

/**
 * Bài 2:
 * Đầu vào: 5 số thực bất kì
 * Các bước xử lí: 
 * + Bước 1: Khai báo 5 biến tương ứng cho 5 số thực
 * + Bước 2: Khai báo biến trungBinh để nhận giá trị là trungBinhCong = (Tổng 5 số)/5 
 * + Bước 3: In kết quả là giá trị biến trungBinhCong theo biểu mẫu ra console.
 * Đầu ra: Giá trị trung bình của 5 số thực
 */
var num1 = 10;
var num2 = 20;
var num3 = 30;
var num4 = 40;
var num5 = 50;
var trungBinhCong = (num1 + num2 + num3 + num4 + num5) / 5;
console.log("Trung bình cộng 5 số:",trungBinhCong);

/**
 * Bài 3:
 * Đầu vào: Giá trị 1USD = 23.500VND, số USD.
 * Các bước xử lí:
 * + Bước 1: Gán giá trị cho biến USD = 23.500
 * + Bước 2: Khai báo biến numUSD là số USD được nhập bất kì.
 * + Bước 3: Tạo biến VND chứa giá trị VND = (USD * numUSD)
 * + Bước 4: In giá trị của biến VND theo biểu mẫu ra console.
 * Đầu ra: Kết quả như biểu mẫu, xuất ra số tiền sau quy đổi VND.
 */
var USD = 23500.00;
var numUSD = 1000;
var VND = (USD * numUSD);
console.log("Quy đổi số USD vừa nhập vào thành (VND):", VND);

/**
 * Bài 4:
 * Đầu vào: Chiều dài, chiều rộng của hình chữ nhật
 * Các bước xử lí:
 * + Bước 1: Tạo biến chieuDai và gán giá trị cho chiều dài 
 * + Bước 2: Tạo biến chieuRong gán giá trị cho chiều rộng 
 * + Bước 3: Tạo biến chuVi nhận giá trị phép tính chuVi = (chieuDai + chieuRong) / 2
 * + Bước 4: Tạo biến dienTich nhận giá trị phép tính dienTich = (chieuDai * chieuRong);
 * + Bước 5: In giá trị chu vi (chuVi) và diện tích (dienTich) theo biểu mẫu ra console.
 * Đầu ra: Diện tích và Chu vi của hình chữ nhật.
 */
var chieuDai = 10;
var chieuRong = 5;
var chuVi = (chieuDai + chieuRong) / 2;
var dienTich = (chieuDai * chieuRong);
console.log("Chu Vi hình chữ nhật:",chuVi);
console.log("Diện tích hình chữ nhật:",dienTich);
 /**
  * Bài 5:
  * Đầu vào: Một số bất kì gồm 2 ký số 
  * Các bước xử lí:
  * + Bước 1: Tạo biến là num có giá trị một số gồm hai ký số
  * + Bước 2: Tạo biến là numDonVi để lấy số hàng đơn vị
  * + Bước 3: Tạo biến là numHangChuc để lấy số hàng chục
  * + Bước 4: Tạo 1 biến là tongHaiKySo để nhận giá trị phép tính  var tongHaiKySo = numDonVi + numHangChuc;
  * + Bước 5: In giá trị tongHaiKySo theo biểu mẫu ra console
  * Đầu ra: Tổng hai ký số của số đã nhập
  */
 var num = 99;
 var numDonVi = num % 10;
 var numHangChuc = (num - numDonVi) / 10;
 var tongHaiKySo = numDonVi + numHangChuc;
 console.log("Tổng hai ký số:",tongHaiKySo);

